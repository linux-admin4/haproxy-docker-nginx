FROM nginx:latest
RUN apt-get update && apt-get install -y curl
COPY default.conf /etc/nginx/conf.d/default.conf
COPY generate_index.sh /usr/local/bin/generate_index.sh
RUN chmod +x /usr/local/bin/generate_index.sh
CMD ["/bin/bash", "-c", "/usr/local/bin/generate_index.sh && nginx -g 'daemon off;'"]

