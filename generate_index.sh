#!/bin/bash

# Получаем хэш контейнера
CONTAINER_HASH=$(hostname)

# Создаем HTML-файл
cat <<EOT > /usr/share/nginx/html/index.html
<!DOCTYPE html>
<html>
<head>
    <title>Hostname</title>
</head>
<body>
    <h1>Hostname: $CONTAINER_HASH</h1>
</body>
</html>
EOT
